<h1 align="center">Hi 👋, I'm Ilham Ariansyah</h1>
<h3 align="center">A Computer Science graduate with a keen interest in Quality Assurance </h3>

- 👨🏽‍💻 Activity **I’m currently participating in a Quality Assurance Bootcamp at Binar Academy**

- 🌱 I’m currently learning **Automation testing**


<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://www.linkedin.com/in/ilhamariansyah57/" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="egidio de-souza" height="30" width="40" 


&nbsp;&nbsp;&nbsp;
<a href="mailto:ilhamariansyah57@gmail.com" target="blank"><img align="center" src="https://mailmeteor.com/logos/assets/PNG/Gmail_Logo_512px.png" alt="egidio de-souza" height="30" width="40" /></a>
</p>
